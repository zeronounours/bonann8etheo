# BonAnn8eTheo

This program is very special for someone. It has turned into a small, simple,
CTF on kubernetes.

I almost forgot: here is one:

```
BonAnn8CTheoF{H0w_did_you_find_m3_?_0S1NT_or_fuzzin_?}
```

## Getting started

## Contributing

All contributions are highly encouraged. See [CONTRIBUTING.md](CONTRIBUTING.md)
for more details. Open-souce projects always need help. You may even be
rewarded if you do so.

For developers, you may consider using [pre-commit](https://pre-commit.com) to
ensure a minimal code-quality before pushing. Start by running:

```
go install golang.org/x/lint/golint@latest
pip install pre-commit
pre-commit install
```

## Authors and acknowledgment

Authors:

- Gauthier SEBAUX (a.k.a zeroNounours)

## License

Under MIT license
