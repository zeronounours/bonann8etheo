#####################################
# Build the go binary
#####################################
FROM docker.io/golang:alpine AS build-go
ARG FLAG
# Install git.
# Git is required for fetching the dependencies.
RUN apk update && apk add --no-cache git gcc musl-dev

WORKDIR $GOPATH/src/gitlab.com/zeronounours/bonann8etheo/
COPY . .
# Fetch dependencies.
# Using go get.
RUN go get -d -v
RUN sed -i "s/FLAG4/${FLAG}/" server/server.go
# Build the binary.
RUN CGO_ENABLED=1 GOOS=linux go build -a -ldflags '-linkmode external -extldflags "-static"' -o /go/bin/bonann8etheo

#####################################
# Create the final image
#####################################
FROM scratch
# Copy our static executable.
COPY --from=build-go /go/bin/bonann8etheo /bin/bonann8etheo
# Run the hello binary.
ENTRYPOINT ["/bin/bonann8etheo"]
