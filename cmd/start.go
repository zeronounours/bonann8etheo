package cmd

/*
Copyright © 2022 zeroNounours

*/

import (
	"github.com/spf13/cobra"

	"gitlab.com/zeronounours/bonann8etheo/server"
)

var (
	// variables used by flags
	address   string
	port      int
	confPath  string
	flag      string
	superFlag string
	uploadDir string
	debug     bool
)

// startCmd represents the start command
var startCmd = &cobra.Command{
	Use:   "start",
	Short: "Start the web server",
	Long: `Start the web server on the given interface and port. It is
possible to modify the config file to use.`,

	RunE: func(cmd *cobra.Command, args []string) error {
		return server.Start(address, port, confPath, flag, uploadDir, superFlag)
	},
}

func init() {
	rootCmd.AddCommand(startCmd)

	// Defines the flags to be used
	startCmd.Flags().StringVarP(&address, "address", "a", "127.0.0.1", "Address to bind the server to")
	startCmd.Flags().IntVarP(&port, "port", "p", 8080, "Port to bind the server to")
	startCmd.Flags().StringVarP(&confPath, "conf-path", "c", "./special_day.txt", "Local config file to use")
	startCmd.Flags().StringVarP(&flag, "flag", "f", "Flag", "Secret flag to give")
	startCmd.Flags().StringVarP(&superFlag, "super-flag", "F", "./superflag.txt", "File containing the super secret flag")
	startCmd.Flags().StringVarP(&uploadDir, "upload", "u", "./uploads", "Path to the uploads directory")
}
