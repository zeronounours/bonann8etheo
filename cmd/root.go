package cmd

/*
Copyright © 2022 zeroNounours

*/

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path"
	"runtime"

	"github.com/spf13/cobra"

	"gitlab.com/zeronounours/bonann8etheo/server"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "bonann8etheo",
	Short: "This program is very special for someone.",
	Run: func(cmd *cobra.Command, args []string) {
		/*
				if run with no arguments, start the serveur with default value and open
			  the browser to it
		*/

		// Get the binary path
		exe, err := os.Executable()
		if err != nil {
			log.Fatalf("Failed to get executable path: %s", err.Error())
		}

		// start the server in a goroutine
		ch := make(chan bool)
		go func() {
			server.Start("127.0.0.1", 8080, path.Join(path.Dir(exe), "special_day.txt"), "my_flag", "./uploads", "./superflag.txt")
			ch <- true
		}()

		// Open the browser
		openbrowser("http://127.0.0.1:8080")

		// wait for the server to be stopped
		<-ch
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func openbrowser(url string) {
	var err error

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}
}
