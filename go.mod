module gitlab.com/zeronounours/bonann8etheo

go 1.18

require github.com/spf13/cobra v1.5.0

require (
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20220908164124-27713097b956 // indirect
)
