package server

/*
Copyright © 2022 zeroNounours

*/

import (
	"context"
	"crypto/sha1"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path"
	"strconv"
	"strings"
	"time"

	"net/http"

	"github.com/fsnotify/fsnotify"
)

var (
	ann8eDate      time.Time
	superPowerFlag string
	ultimateFlag   string
	uploadDir      string
)

// Hard coded secrets are bad…
const superSecret string = "be69bca6-305c-4f6b-a9e4-e9e719850a78"

const ciFlag string = "BonAnn8CTheoF{FLAG4}"

const httpMaxMemory = 200 << 20 // 200 MB

func init() {
	// Create the handlers
	// health handler
	http.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json; charset=utf-8")
		io.WriteString(w, `{"status": true}`)
	})

	// robots.txt
	http.HandleFunc("/robots.txt", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /robots.txt", r.Method, r.URL.String())
		io.WriteString(w, `
User-agent: *
Disallow: /admin
Disallow: /flag
Disallow: /uploads/
`)
	})

	// admin handler
	http.HandleFunc("/admin", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /admin", r.Method, r.URL.String())
		w.WriteHeader(http.StatusForbidden)
		io.WriteString(w, "Nice try!\n")
	})

	// git handler
	gitHandler := func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /.git", r.Method, r.URL.String())
		w.Header().Set("Location", "https://gitlab.com/zeronounours/bonann8etheo")
		w.WriteHeader(http.StatusMovedPermanently)
	}
	http.HandleFunc("/.git", gitHandler)
	http.HandleFunc("/.git/", gitHandler)

	// standard/base handler
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /", r.Method, r.URL.String())
		now := time.Now()
		if now.Day() == ann8eDate.Day() && now.Month() == ann8eDate.Month() {
			io.WriteString(w, "/BonAnn8eTheo! This k8s application is specially for you.\n")
			if now.Day() != 23 || now.Month() != 5 {
				io.WriteString(w, fmt.Sprintf("Wahou! How did you get there? Here is your reward: %s\n", ultimateFlag))
			}
		} else {
			w.WriteHeader(http.StatusServiceUnavailable)
			io.WriteString(w, "Try later.\n")
		}
	})

	// special handler to retrieve the flag
	http.HandleFunc("/flag", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /flag", r.Method, r.URL.String())
		if r.Method != "POST" {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		secret := r.PostFormValue("secret")
		if secret == "" {
			w.WriteHeader(http.StatusBadRequest)
			io.WriteString(w, "Expecting a secret parameter\n")
			return
		}

		if secret == superSecret {
			io.WriteString(w, fmt.Sprintf("Congratulations! Here is your flag: %s\n", superPowerFlag))
		} else {
			io.WriteString(w, "This is not the secret... Try harder\n")
		}
	})

	// handler to server all uploaded files
	http.HandleFunc("/uploads/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /uploads", r.Method, r.URL.String())
		if r.Method != "GET" {
			w.WriteHeader(http.StatusMethodNotAllowed)
			return
		}

		// serve upload files
		// yes, there are path traversal here
		fpath := path.Join(uploadDir, strings.TrimPrefix(r.URL.Path, "/uploads/"))

		// check that the file exist
		stat, err := os.Stat(fpath)
		if err != nil {
			if os.IsNotExist(err) {
				w.WriteHeader(http.StatusNotFound)
			} else {
				log.Printf("failed to stat file: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
			}
			return
		}

		// Check if the file is executable. If it is, run the file and return
		// the output. Else return the file
		var content []byte
		if stat.Mode()&0100 != 0 {
			// executable
			// create command with timeout
			ctx, cancel := context.WithTimeout(context.Background(), time.Duration(5)*time.Second)
			defer cancel()
			cmd := exec.CommandContext(ctx, fpath)

			// run the command
			content, err = cmd.CombinedOutput()
			if err != nil {
				// error in command
				// return output with error 500
				log.Printf("failed to run command: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, string(content))
			}

		} else {
			// file non executable
			// read file content
			content, err = ioutil.ReadFile(fpath)
			if err != nil {
				log.Printf("failed to read file: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}

		// Identify the content type and return the content
		w.Header().Set("Content-Type", http.DetectContentType(content))
		io.WriteString(w, string(content))
	})

	// handler to upload file
	http.HandleFunc("/upload", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("Request %s %s --> /upload", r.Method, r.URL.String())
		if r.Method == "GET" {
			// For GET print instruction to upload
			io.WriteString(w, `To upload a message, use the POST method and file parameter. The mode parametr can be used to control mode on the uploaded file.

For example:
curl <URL> -X POST -F mode=$((0400)) -F file=@myfile.png
`)

		} else if r.Method == "POST" {
			// For POST upload files
			r.ParseMultipartForm(httpMaxMemory)
			srcFile, info, err := r.FormFile("file")
			if err != nil {
				log.Printf("failed to acquire the uploaded content: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Failed upload\n")
				return
			}
			defer srcFile.Close()

			body, err := ioutil.ReadAll(srcFile)
			if err != nil {
				log.Printf("failed to read the uploaded content: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Failed upload\n")
				return
			}

			filename := info.Filename
			if filename == "" {
				filename = fmt.Sprintf("%x", sha1.Sum(body))
			}

			// yes, there are path traversal here
			dstPath := path.Join(uploadDir, filename)
			dstFile, err := os.OpenFile(dstPath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
			if err != nil {
				log.Printf("failed to open the destination file: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Failed upload\n")
				return
			}
			defer dstFile.Close()

			if _, err := dstFile.Write(body); err != nil {
				log.Printf("failed to write the content of destination file: %s", err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				io.WriteString(w, "Failed upload\n")
				return
			}

			mode := r.FormValue("mode")
			if mode != "" {
				if nmode, err := strconv.Atoi(mode); err != nil {
					log.Printf("failed to parse mode of destination file: %s", err.Error())
					io.WriteString(w, "Failed to parse file mode\n")
				} else {
					if err = os.Chmod(dstPath, os.FileMode(nmode)); err != nil {
						log.Printf("failed to change mode of destination file: %s", err.Error())
						io.WriteString(w, "Failed to change file mode\n")
					}
				}
			}

			io.WriteString(w, "The file was successfully uploaded! Use it quickly as it will auto-destroy soon\n")

		} else {
			// Any other ends with method not allowed
			w.WriteHeader(http.StatusMethodNotAllowed)
		}
	})
}

// Start is the function which start the Gin API web server
func Start(address string, port int, confPath string, flag string, upload string, superFlagFile string) error {
	watchConfig(confPath)

	// save flag
	superPowerFlag = flag

	// save upload dir & ensure it exists
	uploadDir = upload
	err := os.MkdirAll(uploadDir, os.ModePerm)
	if err != nil {
		log.Fatalf("Failed to initialize the upload directory: %s", err.Error())
	}
	// Read the super flag file
	content, err := os.ReadFile(superFlagFile)
	if err != nil {
		log.Fatalf("Failed to open the superFlagFile file: %s", err.Error())
	}
	ultimateFlag = string(content)
	// remove the ultimate flag file
	err = os.Remove(superFlagFile)
	if err != nil {
		log.Fatalf("Failed to delete the superFlagFile file: %s", err.Error())
	}

	log.Printf("Using CI/CD flag %s", ciFlag)

	// Start the server
	addr := fmt.Sprintf("%s:%d", address, port)
	log.Printf("Starting server on %s", addr)
	http.ListenAndServe(addr, nil)
	return nil
}

func watchConfig(confPath string) {
	// Create new watcher.
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalln("Failed to create file notifier")
	}

	// Start listening for events.
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				// Ensure receive was successful
				if !ok {
					return
				}

				if event.Has(fsnotify.Write) || event.Has(fsnotify.Remove) {
					// The config text file was modified

					// We reload the file only if it still exists (in case
					// of removed)
					if _, err := os.Stat(event.Name); !os.IsNotExist(err) {
						loadConf(event.Name)

						// Reregister the file to be monitored
						if event.Has(fsnotify.Remove) {
							err = watcher.Add(event.Name)
							if err != nil {
								log.Fatalf("Failed to monitor file `%s'\n", event.Name)
							}
						}
					}
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}

				log.Printf("Error when watching config file: %s\n", err.Error())
			}
		}
	}()

	// Add the config file to the watched files
	err = watcher.Add(confPath)
	if err != nil {
		log.Fatalln("Failed to monitor config file")
	}

	// Load conf initially
	loadConf(confPath)
}

func loadConf(confPath string) {
	log.Printf("Open conf file%s\n", confPath)
	// Read the conf file
	content, err := os.ReadFile(confPath)
	if err != nil {
		log.Fatalf("Failed to open the conf file: %s", err.Error())
	}
	// parse content as date
	ann8eDate, err = time.Parse("02/01\n", string(content))
	if err != nil {
		log.Fatalf("Failed to parse conf file as date: %s", err.Error())
	}
}
