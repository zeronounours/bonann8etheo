/*
Copyright © 2022 zeroNounours
*/
package main

import (
	"gitlab.com/zeronounours/bonann8etheo/cmd"
)

func main() {
	cmd.Execute()
}
